import { Component, ViewChild, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AuthHttp } from 'angular2-jwt';

@Component({
  selector: 'solu-header',
  templateUrl: 'header.component.html',
  styleUrls: ['header.component.less']
})
export class HeaderComponent {
  public logoDisplay = true;
  public initials = '??';

  public constructor() {
      
  }

  /**
   * Initialize the header by retrieving the user's initials
   */
  ngOnInit() {

  }
  
  logoClicked() {
    console.log("main menu button clicked");
  }
  
  
  initialsClicked() {
    console.log("Initials clicked");
  }
}
