// Angular modules and components
import { NgModule, ApplicationRef, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



// Material Components
import { MdSidenavModule } from '@angular/material';
import { MdInputModule } from '@angular/material';
import { MdSelectModule } from '@angular/material';
import { MaterialModule } from '@angular/material';
import { MdButtonModule } from '@angular/material';
import { MdSnackBarModule } from '@angular/material';
import { MdNativeDateModule } from '@angular/material';

import { AppComponent } from './app.component';
import { HeaderComponent } from './solu-header/header.component';

import { InitialSymbolComponent } from './initial-symbol/initial-symbol.component';

/**
 * Declaration of the CM micro-service application module.
 */
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    InitialSymbolComponent,
  ],

  entryComponents: [
  ],

  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    BrowserAnimationsModule,
    MdSidenavModule,
    MdInputModule,
    MdSelectModule,
    MaterialModule,
    MdNativeDateModule,
  ],
  providers: [
  ],  // end of providers
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule { }
