import { Component, OnInit, Input } from '@angular/core';

/**
 * The UI widget that displays an icon of some user's initials.
 */
@Component({
  selector: 'solu-initial-symbol',
  templateUrl: './initial-symbol.component.html',
  styleUrls:['./initial-symbol.component.less']
})
export class InitialSymbolComponent implements OnInit {

  @Input() size: string = "large";
  @Input() initials: string;

  public iconClass: string;

  /**
   * Initialize the view-model of this component.
   */
  ngOnInit() {
    //set the class of the button according to the input
    this.iconClass = this.generateClassName();
  }

  /**
   * Generate a CSS class name for this component.
   * It's based upon the size input.
   */
  private generateClassName(){
    return 'icon__' + this.size;
  }
}
