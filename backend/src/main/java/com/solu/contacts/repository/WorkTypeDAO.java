/**
* Copyright 2017: Solu Technology Partners
* All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
* Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
*/
package com.solu.contacts.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.solu.contacts.domain.WorkType;

/**
 * The Data Access Object contract for {@link WorkType} entities.
 * 
 * @author <a href='mailto:bsentiff@solutechnology.com'>Ben Sentiff</a>
 */
public interface WorkTypeDAO extends JpaRepository<WorkType, Integer> {

}