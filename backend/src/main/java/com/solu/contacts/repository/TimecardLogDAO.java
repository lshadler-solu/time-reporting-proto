/**
 * Copyright 2015: Solu Technology Partners
 * All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
 * Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
 */
package com.solu.contacts.repository;

import java.util.Date;
import java.util.List;

import com.solu.contacts.domain.TimecardLog;
import com.solu.contacts.domain.WorkType;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * The Data Access Object contract for {@link TimecardLog} entities.
 *
 * @author <a href='mailto:bbasham@solutechnology.com'>Bryan Basham</a>
 */
public interface TimecardLogDAO extends JpaRepository<TimecardLog, Integer> {

    List<TimecardLog> findAllByDate(Date date);

    List<TimecardLog> findAllByWorkType(WorkType workType);

}
