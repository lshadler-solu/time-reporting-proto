/**
* Copyright 2015-2017: Solu Technology Partners
* All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
* Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
*/

package com.solu.contacts.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonView;

import com.solu.domain.AbstractDomainEntity;

/**
 * Each Log indicates the number of hours performing an action on a given day.
 * 
 * 
 * @author <a href='mailto:lshadler@solutechnology.com'>Lucas Shadler</a>

 */
@Entity
@Table(name = "timecard_log")
public class TimecardLog extends AbstractDomainEntity {


  /**
   * The marker interface used by the JSON conversion tool to extract
   * only the Contact Mgmt related {@link Assignment} properties.
   */
  public interface TimecardLogView extends Assignment.AssignmentView {}

  //
  // Attributes
  //

  @Id
  @Column(name = "id")
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private Long id;
  
  @Column(name="date",nullable=false)
  private Date date;

  @Column(name="hours_worked")
  private Double hoursWorked;
  
  @OneToOne(fetch=FetchType.EAGER, optional=true)
  @JoinColumn(name="assignment_id")
  private Assignment assignment;

  @OneToOne(fetch=FetchType.EAGER, optional=true)
  @JoinColumn(name="work_type_id")
  private WorkType workType;
  //
  // Constructors
  //

  /**
   * The no-arg ctor for Serialization and JPA.
   */
  TimecardLog() {}

  //
  // Accessors
  //

  @Override
  @JsonView(TimecardLog.TimecardLogView.class)
  public Long getId() {
    return id;
  }

  @JsonView(TimecardLog.TimecardLogView.class)
  public Date getDate() {
    return date;
  }

  @JsonView(TimecardLog.TimecardLogView.class)
  public void setDate(Date date) {
    this.date = date;
  }

  @JsonView(TimecardLog.TimecardLogView.class)
  public Double getHoursWorked() {
    return hoursWorked;
  }

  @JsonView(TimecardLog.TimecardLogView.class)
  public void setHoursWorked(Double hoursWorked) {
    this.hoursWorked = hoursWorked;
  }

  public Assignment getAssignment() {
    return assignment;
  }

  @JsonView(TimecardLog.TimecardLogView.class)
  public void setAssignment(Assignment assignment) {
    this.assignment = assignment;
  }

  @JsonView(TimecardLog.TimecardLogView.class)
  public WorkType getWorkType() {
    return workType;
  }

  @JsonView(TimecardLog.TimecardLogView.class)
  public void setWorkType(WorkType workType) {
    this.workType = workType;
  }
  
  //
  // Object methods
  //

  @Override
  public String toString() {
    return toStringHelper(Assignment.class).toString();
  }

  @Override
  public boolean equals(Object obj) {
    return equals(obj, Assignment.class);
  }
}
