package com.solu.contacts.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * A WorkType Entity
 * @author Ben Sentiff
 *
 */
@Entity
@Table(name="work_type")
public class WorkType extends AbstractReferenceEntity{  
  //
  // Object methods
  //

  @Override
  public String toString() {
    return toStringHelper(WorkType.class).toString();
  }

  @Override
  public boolean equals(Object obj) {
    return equals(obj, Title.class);
  }
}