/**
 * Copyright 2015 - 2017: Solu Technology Partners
 * All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
 * Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
 */
package com.solu.contacts.web;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import java.util.List;

import javax.servlet.ServletException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.solu.contacts.domain.WorkType;
import com.solu.contacts.service.WorkTypeService;

/**
 * The RESTful service for {@link WorkType} entities.
 * 
 * @author <a href="mailto: bsentiff@solutechnology.com">Ben Sentiff</a>
 */
@RestController
@RequestMapping(value = "/api/workTypes")
public class WorkTypeRestController {

  @Autowired
  private WorkTypeService workTypeSvc;
  
  @RequestMapping(method = GET)
  @JsonView(WorkType.class)
  public List<WorkType> list() throws ServletException {
    return workTypeSvc.retrieveAll();
  }
}
