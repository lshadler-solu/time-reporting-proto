/**
 * Copyright 2015 - 2017: Solu Technology Partners
 * All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
 * Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
 */
package com.solu.contacts.web;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import java.util.List;

import javax.servlet.ServletException;

import com.solu.contacts.domain.TimecardLog;
import com.solu.contacts.service.TimecardLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.solu.contacts.domain.EmploymentDescription;
import com.solu.contacts.service.EmploymentDescriptionService;

/**
 * The RESTful service for {@link EmploymentDescription} entities.
 *
 * @author <a href='mailto:lshadler@solutechnology.com'>Lucas Shadler</a>
 */
@RestController
@RequestMapping(value = "/api/timecard-log")
public class TimecardLogRestController {

    @Autowired
    private TimecardLogService timecardLogService;

    @RequestMapping(method = GET)
    @JsonView(TimecardLog.TimecardLogView.class)
    public List<TimecardLog> list() throws ServletException {
        return timecardLogService.retrieveAll();
    }
}