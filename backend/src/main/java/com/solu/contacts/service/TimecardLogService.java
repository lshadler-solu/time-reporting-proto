package com.solu.contacts.service;

import com.solu.contacts.domain.TimecardLog;
import com.solu.contacts.repository.TimecardLogDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class TimecardLogService {

    //
    // Dependency Injection
    //

    @Autowired
    private TimecardLogDAO timecardLogDAO;

    //
    // EmploymentDescriptionService methods
    //

    /**
     * Retrieves all {@linkplain TimecardLog logs}
     * ordered by name in descending order.
     *
     * @return a complete, sorted list of employmentDescription types
     */
    @Transactional(readOnly = true)
    public List<TimecardLog> retrieveAll() {
        Sort s = new Sort(Sort.Direction.ASC, "id");
        return timecardLogDAO.findAll(s);
    }

    /**
     * Retrieves all {@linkplain TimecardLog logs}
     * by a given date
     *
     * @return a complete, sorted list of employmentDescription types
     */
    @Transactional(readOnly = true)
    public List<TimecardLog> retriveAllByDate(Date date) {
        return timecardLogDAO.findAllByDate(date);
    }

}
