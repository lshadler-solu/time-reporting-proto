/**
* Copyright 2017: Solu Technology Partners
* All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
* Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
*/
package com.solu.contacts.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.solu.contacts.domain.WorkType;
import com.solu.contacts.repository.WorkTypeDAO;

/**
 * The Service to manage {@link WorkType} entities.
 * 
 * @author <a href="mailto: bsentiff@solutechnology.com">Ben Sentiff</a>
 */
@Service
public class WorkTypeService {

  //
  // Dependency Injection
  //

  @Autowired
  private WorkTypeDAO workTypeDAO;

  //
  // TitleService methods
  //

  /**
   * Retrieves all {@linkplain WorkType workTypes}
   * ordered by name in descending order.
   * 
   * @return a complete, sorted list of workTypes
   */
  @Transactional(readOnly = true)
  public List<WorkType> retrieveAll() {
    Sort s = new Sort(Direction.ASC, "name");
    return workTypeDAO.findAll(s);
  }
}
