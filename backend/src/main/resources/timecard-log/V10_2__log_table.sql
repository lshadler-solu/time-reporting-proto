--
--  Create a log table for timecards
--

DROP TABLE IF EXISTS `timecard_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timecard_log` (
  `id` BIGINT UNSIGNED AUTO_INCREMENT,
  `date` DATE NOT NULL,
  `assignment_id` BIGINT UNSIGNED NOT NULL,
  `work_type_id` INT UNSIGNED,
  `hours_worked` DOUBLE,
  PRIMARY KEY(`id`),
  
  CONSTRAINT `log_assignment_id` 
    FOREIGN KEY (`assignment_id`)
     REFERENCES `assignment` (`id`) 
      ON DELETE NO ACTION 
      ON UPDATE NO ACTION,
      
  CONSTRAINT `log_work_type_id` 
    FOREIGN KEY (`work_type_id`)
     REFERENCES `work_type` (`id`) 
      ON DELETE NO ACTION 
      ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SHOW ENGINE INNODB STATUS;