/*
 * Refactor the person object relationships in order to add in the new concept of assignment.
 */

/* DDL Code */

CREATE TABLE `work_type` (
  `id` INT UNSIGNED AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`));

INSERT INTO `work_type` (`name`) VALUES ('Functional Work');
INSERT INTO `work_type` (`name`) VALUES ('Project Work');
INSERT INTO `work_type` (`name`) VALUES ('Root Cause APRs');
INSERT INTO `work_type` (`name`) VALUES ('Business As Usual');
INSERT INTO `work_type` (`name`) VALUES ('Time Away');
